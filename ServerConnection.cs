using System;
using System.Collections;
using System.IO;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.Threading;

namespace TikiRC.Gui
{
	/// <summary>
	/// Summary description for ServerConnection.
	/// </summary>
	public class ServerConnection
	{
		// later to be a mentalis virtual something-or-other
		private TcpClient client;
		private StreamReader reader;
		private StreamWriter writer;
		private bool connected = false;

		private string servername;
		private int port;
		private string nickname;
		private string username;
		private string hostname;
		private string realname;
		private string password = "";
		private string userhost = "";

		private Hashtable channels;
		private Hashtable queries;
		private StatusWindow status;

		// regular expressions: (borrowed and modified from the SmartIrc4Net lib.)
		private static Regex action = new Regex("^:.*? PRIVMSG (.).* :" + "\x1" + "ACTION .*" + "\x1" + "$", RegexOptions.Compiled);
		private static Regex ctcpRequest = new Regex("^:.*? PRIVMSG .* :" + "\x1" + ".*" + "\x1" + "$", RegexOptions.Compiled);
		private static Regex privmsg = new Regex("^:.*? PRIVMSG (.).* :.*$", RegexOptions.Compiled);
		private static Regex ctcpReply = new Regex("^:.*? NOTICE .* :" + "\x1" + ".*" + "\x1" + "$", RegexOptions.Compiled);
		private static Regex notice = new Regex("^:.*? NOTICE (.).* :.*$", RegexOptions.Compiled);
		private static Regex invite = new Regex("^:.*? INVITE .* .*$", RegexOptions.Compiled);
		private static Regex join = new Regex("^:(.*) JOIN :(.*)$", RegexOptions.Compiled);
		private static Regex topic = new Regex("^:.*? TOPIC .* :.*$", RegexOptions.Compiled);
		private static Regex nick = new Regex("^:.*? NICK .*$", RegexOptions.Compiled);
		private static Regex kick = new Regex("^:.*? KICK .* .*$", RegexOptions.Compiled);
		private static Regex part = new Regex("^:.*? PART .*$", RegexOptions.Compiled);
		private static Regex mode = new Regex("^:.*? MODE (.*) .*$", RegexOptions.Compiled);
		private static Regex quit = new Regex("^:.*? QUIT :.*$", RegexOptions.Compiled);

		public ServerConnection(string servername, int port, string nickname, string username, string hostname, string realname, string password, StatusWindow status)
		{
			this.servername = servername;
			this.port = port;
			this.nickname = nickname;
			this.username = username;
			this.hostname = hostname;
			this.realname = realname;
			this.password = password;
			this.status = status;

			channels = new Hashtable();
			queries = new Hashtable();
			this.status.ServerConnection = this;

			// todo: set all the values first
			// make sure we know the difference between username and nickname, and that hostname is "email" for some odd reason, or the site we're connecting from
			//Connect();

		}

		public void Connect()
		{
			try 
			{
				client = new TcpClient(servername, port);
				reader = new StreamReader(client.GetStream());
				writer = new StreamWriter(client.GetStream());
				Thread t = new Thread(new ThreadStart(this.Read));
				t.Name = "IrcReader";
				t.Start();
				// since we're reading everything, we can just go ahead and send command, since it's asynchronous
				
				connected = Login();
				Logger.Log("Connected to irc...");
			} 
			catch
			{
				Logger.Log("Connection to " + servername + ":" + port + " failed, retrying.");
				Thread.Sleep(5000);
				Connect();
			}
		}

		public void Disconnect()
		{
			connected = false;
			client.Close();
		}
		public bool Login()
		{
			WriteLine("NICK " + nickname);
			WriteLine("USER " + username + " \"" + hostname + "\" \"" + servername + "\" :" + realname);
			if (password.Length > 0)
			{
				WriteLine("PASS " + password);
			}
			WriteLine("USERHOST " + nickname);
			return true;
		}

		public void SendMessage(string message)
		{
			// write out the proper format here, maybe we'll take more parameters later
			// OBS! max 510 chars
			message = message.Substring(0, Math.Min(510, message.Length));
			WriteLine(message);
		}
		private void WriteLine(string line)
		{
			Logger.Log("SENDING: " + line);
			writer.Write(line + "\n\n");
			writer.Flush();
		}

		private void Read()
		{
			string line = "";
			while ((line = reader.ReadLine()) != null)
			{
				Logger.Log("READING: " + line);
				HandleReponse(line);
			}
		}

		private void HandleReponse(string response)
		{
			//m�ste kunna hantera:
			// queries (PRIVMSG)
			// channels
			// notice
			// ctcp
			// (dcc)

			// kolla alla sorters svar vi kan f�, parsa dom, och g�r r�tt sorts response-objekt

			// kolla vilken userhost vi har, s� vi kan anv�nda det f�r att parsa joins till kanaler
			
			status.Log(response);

			// AHH, if the message starts with a ":", then it will be in the "proper" format, and we can generalize based on that.
			string[] data = response.Split(' ');
			if (response.StartsWith(":"))
			{
				if (data.Length > 1) {
					int rc = 0;
					// data[0] is the sender, i.e the server, or a userhost from another user
					string responseName = data[1];
					try 
					{
						rc = int.Parse(responseName);
						// have a switch-case statement here that takes care of the numerical reponses
					}
					catch (FormatException e)
					{
						// the response wat not numerical, so it'll be a "JOIN" or a "PRIVMSG" or something along those lines.
						// have a switch-case statement here that takes care of the string replies
						switch (responseName.ToUpper())
						{
							case "JOIN":
								HandleJoin(response);
								break;
							case "PRIVMSG":
								HandlePrivmsg(response);
								break;

						}
					}
				}
			}
			else
			{
				if (data[0].Equals("PING"))
				{
					// return a PONG with everything we got as a parameter
					SendMessage("PONG " + response.Substring(5));
				}
				else if (data[0].Equals("ERROR"))
				{
					
				}
				else
				{
					// is ping the only command shaped like that? if so, then we should eb able to generalize from here.
					// ok, NOTICE is also shaped like that
				}
			}
		}
		/*
		HandleWhoResponse or something. (352)
		// create user objects here
		// set the active server for them, or rather
		*/
		private void HandlePrivmsg(string response)
		{
			string[] data = response.Split(' ');
			if (data.Length > 3)
			{
				User sender = new User(data[0]);
				string destination = data[2];
				// remove the first ":"
				response = response.Substring(1);
				string message = response.Substring(response.IndexOf(":"));
				if (destination.StartsWith("#") || destination.StartsWith("&"))
				{
					// destination is a channel
					Channel chan = (Channel)channels[destination];
					if (chan != null)
					{
						chan.Say(message, sender);
					}
					else
					{
						Logger.Log("we got a privmsg for a channel we haven't joined: " + response);
					}
				}
				else
				{
					// it's a MSG/query
					// sent to me (how could it even be sent to anybody else?)
					Query query = (Query)queries[sender.Nickname];
					if (query == null)
					{
						query = new Query(sender, this);
						queries[sender.Nickname] = query;
					}
					query.Say(message, sender);
				}
			}

		}

		private void HandleJoin(string response)
		{
			// create new channelwindow
			// which in turn executes a who and names and such
			// add the channel to the set of channels
			// if we already have the channel, add the user to the channel
			string channelName = null;
			string userHost;
			string[] data = response.Split(' ');
			if (data.Length == 3)
			{
				// the first entry, minus the preceding ":"
				userHost = data[0].Substring(1);
				channelName = data[2].Substring(1);
				User user = new User(userHost);
				Channel chan = (Channel)channels[channelName];
				// todo: this needs support for multiple servers maybe? (or not, that might only be a thing when we're working towards a bnc)
				if (chan == null)
				{
					channels[channelName] = status.CreateChannel(channelName);
				}
				else
				{
					chan.Join(user);	
				}
			}
			else
			{
				throw new ArgumentException("join message was not properly formatted: " + response);
			}
		}
	}
}
