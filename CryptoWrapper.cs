/**
 * Copyright (c) 2004, Markus Jevring <jevring@gmail.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
using System;
using System.Runtime.InteropServices;

namespace TikiRC
{
	/// <summary>
	/// This is a C# .net wrapper for the mircryption dll from http://mircryption.sourceforge.net.
	/// Written by Markus Jevring <jevring@gmail.com>.
	/// Licensed under the BSD license.
	/// </summary>
	public class CryptoWrapper
	{
		[DllImport("mircryption.dll")]
		public static extern string DecryptString( [MarshalAs (UnmanagedType.AnsiBStr)] string password,[MarshalAs (UnmanagedType.AnsiBStr)] string cryptedtext,[MarshalAs (UnmanagedType.AnsiBStr)] string plaintext,int maxresultlen);
		
		[DllImport("mircryption.dll")]
		public static extern string EncryptString( [MarshalAs (UnmanagedType.AnsiBStr)] string password,[MarshalAs (UnmanagedType.AnsiBStr)] string plaintext,[MarshalAs (UnmanagedType.AnsiBStr)] string crypttext,int maxresultlen);
		
		[DllImport("mircryption.dll")]
		public static extern void FreeResultString([MarshalAs (UnmanagedType.AnsiBStr)] string text);
		
		[DllImport("mircryption.dll")]
		public static extern string GetVersionString();
		/*
		 * 
		 * string s = CryptoWrapper.DecryptString("test", "QRNyF07meyA0ATXPs12DzDo/U6kg21AAYIF.FzI8B.bhUmf.w00EN0t6pJt.3eAqD.7wKVw.fwESx1gfL0L/Prv6E1DLXxs.ccNbQ.g6E860Cziy80.ZLj30", null, 0);
		 * MessageBox.Show(s);
		 * CryptoWrapper.FreeResultString(s);
		 * .FreeResultString(s); needs to be called after EACH decrypt or encrypt!
		 * 
		 * or do something like this:
		 * 
		 * string mymessage[1000];
		 * CryptoWrapper.DecryptString("test", "QRNyF07meyA0ATXPs12DzDo/U6kg21AAYIF.FzI8B.bhUmf.w00EN0t6pJt.3eAqD.7wKVw.fwESx1gfL0L/Prv6E1DLXxs.ccNbQ.g6E860Cziy80.ZLj30", mymessage, 999);
		 * 
		 * Then you don't have to free the mymessage, you can keep on passing that.
		 * 
		 */

	}
}
