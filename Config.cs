using System;
using System.Collections;
using System.IO;

namespace TikiRC
{
	/// <summary>
	/// Summary description for Config.
	/// </summary>
	public class Config
	{
		private static Hashtable settings = new Hashtable();

		public static void Save()
		{
			try 
			{
				StreamWriter file = new StreamWriter("tikirc.conf");
				foreach (DictionaryEntry setting in settings)
				{
					file.WriteLine(setting.Key + "=" + setting.Value);
				}
				file.Close();
			}
			catch
			{
				Logger.Log("settings file could not be opened.");
			}
		}

		public static void Load()
		{
			try 
			{
				StreamReader file = new StreamReader("tikirc.conf");
				string line = "";
				while ((line = file.ReadLine()) != null)
				{
					int settingSeparatorIndex = line.IndexOf("=");
					string key = line.Substring(0, settingSeparatorIndex).Trim();
					string value = line.Substring(settingSeparatorIndex + 1).Trim();
					settings[key] = value;
				}
				file.Close();
			}
			catch
			{
				Logger.Log("settings file could not be opened");
			}
		}

		public static int GetInt(string key)
		{
			return Convert.ToInt32(Get(key));
		}

		public static void Set(string key, string value)
		{
			Logger.Log("saving: " + key + "=" + value);
			settings[key] = value;
		}

		public static string Get(string key)
		{
			return (string)settings[key];
		}

	}
}
