using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace TikiRC.Gui
{
	/// <summary>
	/// Summary description for Channel.
	/// </summary>
	public class Query : System.Windows.Forms.Form
	{
		private System.Windows.Forms.TextBox input;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		private ServerConnection server;
		private System.Windows.Forms.RichTextBox messages;
		private User user;

		public Query(User user, ServerConnection server)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			// fix channel modes and such later aswell
			this.Text = user.Nickname + "!" + user.Ident + "@" + user.Host;
			this.server = server;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.input = new System.Windows.Forms.TextBox();
			this.messages = new System.Windows.Forms.RichTextBox();
			this.SuspendLayout();
			// 
			// input
			// 
			this.input.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.input.Location = new System.Drawing.Point(0, 817);
			this.input.Name = "input";
			this.input.Size = new System.Drawing.Size(760, 20);
			this.input.TabIndex = 0;
			this.input.Text = "";
			// 
			// messages
			// 
			this.messages.Dock = System.Windows.Forms.DockStyle.Fill;
			this.messages.Location = new System.Drawing.Point(0, 0);
			this.messages.Name = "messages";
			this.messages.ReadOnly = true;
			this.messages.RightMargin = 2;
			this.messages.Size = new System.Drawing.Size(760, 817);
			this.messages.TabIndex = 3;
			this.messages.Text = "";
			// 
			// Query
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(760, 837);
			this.Controls.Add(this.messages);
			this.Controls.Add(this.input);
			this.Name = "Query";
			this.Text = "Query";
			this.ResumeLayout(false);

		}
		#endregion

		public void Say(string line)
		{
			// skriv in inneh�llet i querien
			// skicka det till servern
			server.SendMessage("PRIVMSG " + user.Nickname + ":" + line);
		}

		public void Say(string message, User user)
		{
			// eller kanske "User user", ha ett speciellt objekt eller n�tt, kanske, jag vet inte
			/*
			F�rgl�ggning:
			richTextBox1.SelectionFont=f;
			richTextBox1.AppendText(ff.GetName(1)+"\r\n");
			richTextBox1.SelectionFont=f;
			richTextBox1.AppendText("abcdefghijklmnopqrstuvwxyz\r\n");
			richTextBox1.SelectionFont=f;
			richTextBox1.AppendText("ABCDEFGHIJKLMNOPQRSTUVWXYZ\r\n");
			*/
		}

	}
}
