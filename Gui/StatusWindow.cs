using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;

namespace TikiRC.Gui
{
	public delegate Channel ChannelCreationDelegate(string channelname);

	/// <summary>
	/// Summary description for StatusWindow.
	/// </summary>
	public class StatusWindow : System.Windows.Forms.Form
	{
		private System.Windows.Forms.TextBox input;
		private System.Windows.Forms.RichTextBox status;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		private ServerConnection serverConnection = null;
		private ArrayList previousCommands;
		private int currentPreviousCommandIndex = 0;
		
		public StatusWindow()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			previousCommands = new ArrayList();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.input = new System.Windows.Forms.TextBox();
			this.status = new System.Windows.Forms.RichTextBox();
			this.SuspendLayout();
			// 
			// input
			// 
			this.input.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.input.Location = new System.Drawing.Point(0, 825);
			this.input.Name = "input";
			this.input.Size = new System.Drawing.Size(624, 20);
			this.input.TabIndex = 0;
			this.input.Text = "";
			this.input.KeyUp += new System.Windows.Forms.KeyEventHandler(this.input_KeyUp);
			// 
			// status
			// 
			this.status.Dock = System.Windows.Forms.DockStyle.Fill;
			this.status.Location = new System.Drawing.Point(0, 0);
			this.status.Name = "status";
			this.status.ReadOnly = true;
			this.status.Size = new System.Drawing.Size(624, 825);
			this.status.TabIndex = 1;
			this.status.Text = "";
			// 
			// StatusWindow
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(624, 845);
			this.Controls.Add(this.status);
			this.Controls.Add(this.input);
			this.Name = "StatusWindow";
			this.Text = "StatusWindow";
			this.ResumeLayout(false);

		}
		#endregion


		public void Log(string message)
		{
			// later we'll colorize everything properly
			status.AppendText(message + Environment.NewLine);
		}


		// maybe we should move these 2 to something that all types of windows inherit from.
		private void input_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.Enter:
					// send the command
					if (input.Text.StartsWith("/"))
					{
						// this is a command, send it, without the /, to the server.
						// well, not really, we need to parse the command first to check what is happening
						HandleCommand(input.Text.Substring(1));
					}
					previousCommands.Add(input.Text);
					currentPreviousCommandIndex = previousCommands.Count;
					input.Clear();
					break;
				case Keys.Up:
					DosKey(true);
					break;
				case Keys.Down:
					DosKey(false);
					break;
			}
		}
		private void DosKey(bool up)
		{
			// todo: make sure that if we use a command, that we don't just add it to doskey, but we move it to a better position

			if (up)
			{
				currentPreviousCommandIndex++;
			}
			else
			{
				currentPreviousCommandIndex--;
			}
			if (currentPreviousCommandIndex >= previousCommands.Count || currentPreviousCommandIndex < 0)
			{
				currentPreviousCommandIndex = 0;
			}
			if (previousCommands.Count > 0) 
			{
				input.Text = (string)previousCommands[currentPreviousCommandIndex];
			}
			
		}


		// todo: move this to a better place?
		public void HandleCommand(string command)
		{
			string[] commandParameters = command.Split(' ');
			switch (commandParameters[0].ToLower())
			{
				case "server":
				case "connect":
					ConnectToServer(commandParameters);
					break;
				case "save":
					Log("Saving settings...");
					Config.Save();
					break;
				case "join":
					serverConnection.SendMessage("JOIN " + command.Substring(5));
					break;
				case "j":
					serverConnection.SendMessage("JOIN " + command.Substring(2));
					break;
				default:
					if (serverConnection != null) 
					{
						serverConnection.SendMessage(command);
					}
					else
					{
						Log("Unsupported command: " + command);
					}
					break;
			}
		}

		private void ConnectToServer(string[] parameters)
		{
			// this might be a bit ugly, and the actions are performed by the "wrong" window, fix this later

			// takes params: new - crates a new status window and a new connection for that status window.
			if (parameters[1].ToLower().Equals("new"))
			{
				StatusWindow sw = new StatusWindow();
				sw.MdiParent = this.MdiParent;
				// check the parameters first, so they are proper, but for now, just do it.
				int port = 6667;
				if (parameters.Length > 3) 
				{
					try
					{
						port = Convert.ToInt32(parameters[3]);
					}
					catch (FormatException e)
					{
						// do nothing, just connect anyway
						// handle stuff like server password here later, or something
					}
				}
				ServerConnection sc = new ServerConnection(parameters[2], port, Config.Get("irc.nickname"), Config.Get("irc.username"), Config.Get("irc.hostname"), Config.Get("irc.realname"), Config.Get("irc.password"), sw);
				sw.Log("Connecting to server " + parameters[2] + " on port " + port);
				//sc.Connect();
				sw.Show();
				Thread t = new Thread(new ThreadStart(sc.Connect));
				t.Start();
				
			}
			else
			{
				int port = 6667;
				if (parameters.Length > 2) 
				{
					try
					{
						port = Convert.ToInt32(parameters[2]);
					}
					catch (FormatException e)
					{
						// do nothing, just connect anyway
						// handle stuff like server password here later, or something
					}
				}
				ServerConnection sc = new ServerConnection(parameters[1], port, Config.Get("irc.nickname"), Config.Get("irc.username"), Config.Get("irc.hostname"), Config.Get("irc.realname"), Config.Get("irc.password"), this);
				Log("Connecting to server " + parameters[1] + " on port " + port);
				//sc.Connect();
				Thread t = new Thread(new ThreadStart(sc.Connect));
				t.Start();
				
			}
		}

		public Channel CreateChannel(string channelName)
		{
			ChannelCreationDelegate ccd = new ChannelCreationDelegate(NewChannel);
			//this.Invoke() // <- use this together with something to tell the parent thread to create the channel.
			Channel chan = (Channel)this.Invoke(ccd, new object[]{channelName});
			// this looks like a half-assed hack way to create a new mdi-child from a different thread.
			// pretty fucking ludicrous if you ask me
			return chan;
		}
		private Channel NewChannel(string channelName)
		{
			Channel chan = new Channel(channelName, serverConnection);
			chan.MdiParent = this.MdiParent;
			chan.Show();
			chan.Initiate();
			return chan;
			
		}
		
		
		public ServerConnection ServerConnection
		{
			get { return serverConnection; }
			set { serverConnection = value; }
		}

	}
}
