using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace TikiRC.Gui
{
	/// <summary>
	/// Summary description for Settings.
	/// </summary>
	public class Settings : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox nickname;
		private System.Windows.Forms.TextBox username;
		private System.Windows.Forms.TextBox realname;
		private System.Windows.Forms.Label passwordLabel;
		private System.Windows.Forms.TextBox password;
		private System.Windows.Forms.Button okButton;
		private System.Windows.Forms.Button cancelButton;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox hostname;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Settings()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.nickname = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.username = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.realname = new System.Windows.Forms.TextBox();
			this.passwordLabel = new System.Windows.Forms.Label();
			this.password = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.okButton = new System.Windows.Forms.Button();
			this.cancelButton = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.hostname = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// nickname
			// 
			this.nickname.Location = new System.Drawing.Point(16, 24);
			this.nickname.Name = "nickname";
			this.nickname.TabIndex = 0;
			this.nickname.Text = "";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(136, 24);
			this.label1.Name = "label1";
			this.label1.TabIndex = 1;
			this.label1.Text = "Nickname";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(136, 56);
			this.label2.Name = "label2";
			this.label2.TabIndex = 3;
			this.label2.Text = "Username";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// username
			// 
			this.username.Location = new System.Drawing.Point(16, 56);
			this.username.Name = "username";
			this.username.TabIndex = 2;
			this.username.Text = "";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(136, 88);
			this.label3.Name = "label3";
			this.label3.TabIndex = 5;
			this.label3.Text = "Realname";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// realname
			// 
			this.realname.Location = new System.Drawing.Point(16, 88);
			this.realname.Name = "realname";
			this.realname.TabIndex = 4;
			this.realname.Text = "";
			// 
			// passwordLabel
			// 
			this.passwordLabel.Location = new System.Drawing.Point(136, 120);
			this.passwordLabel.Name = "passwordLabel";
			this.passwordLabel.Size = new System.Drawing.Size(136, 23);
			this.passwordLabel.TabIndex = 7;
			this.passwordLabel.Text = "Password (blank if none)";
			this.passwordLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// password
			// 
			this.password.Location = new System.Drawing.Point(16, 120);
			this.password.Name = "password";
			this.password.TabIndex = 6;
			this.password.Text = "";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(400, 200);
			this.label5.Name = "label5";
			this.label5.TabIndex = 9;
			this.label5.Text = "Ident (comes later)";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// okButton
			// 
			this.okButton.Location = new System.Drawing.Point(96, 528);
			this.okButton.Name = "okButton";
			this.okButton.TabIndex = 10;
			this.okButton.Text = "Ok";
			this.okButton.Click += new System.EventHandler(this.okButton_Click);
			// 
			// cancelButton
			// 
			this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cancelButton.Location = new System.Drawing.Point(272, 536);
			this.cancelButton.Name = "cancelButton";
			this.cancelButton.TabIndex = 11;
			this.cancelButton.Text = "Cancel";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(136, 152);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(136, 23);
			this.label4.TabIndex = 13;
			this.label4.Text = "Hostname";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// hostname
			// 
			this.hostname.Location = new System.Drawing.Point(16, 152);
			this.hostname.Name = "hostname";
			this.hostname.TabIndex = 12;
			this.hostname.Text = "";
			// 
			// Settings
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(528, 629);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.hostname);
			this.Controls.Add(this.cancelButton);
			this.Controls.Add(this.okButton);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.passwordLabel);
			this.Controls.Add(this.password);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.realname);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.username);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.nickname);
			this.Name = "Settings";
			this.Text = "Settings";
			this.Load += new System.EventHandler(this.Settings_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private void Settings_Load(object sender, System.EventArgs e)
		{
			// load settings into fields here
			this.username.Text = Config.Get("irc.username");
			this.nickname.Text = Config.Get("irc.nickname");
			this.password.Text = Config.Get("irc.password");
			this.realname.Text = Config.Get("irc.realname");
			this.hostname.Text = Config.Get("irc.hostname");
		}

		private void okButton_Click(object sender, System.EventArgs e)
		{
			// save settings here
			Config.Set("irc.username", this.username.Text);
			Config.Set("irc.nickname", this.nickname.Text);
			Config.Set("irc.password", this.password.Text);
			Config.Set("irc.realname", this.realname.Text);
			Config.Set("irc.hostname", this.hostname.Text);

			this.Close();
		}
	}
}
