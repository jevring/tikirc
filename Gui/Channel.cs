using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace TikiRC.Gui
{
	/// <summary>
	/// Summary description for Channel.
	/// </summary>
	public class Channel : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Splitter splitter1;
		private System.Windows.Forms.TextBox input;
		private System.Windows.Forms.ListBox users;
		private System.Windows.Forms.RichTextBox channelText;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		private ServerConnection server;
		private string channelName = "";
		private int currentPreviousCommandIndex = 0;
		private ArrayList previousCommands;

		public Channel(string channelName, ServerConnection server)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			// fix channel modes and such later aswell
			this.Text = channelName;
			this.channelName = channelName;
			this.server = server;
			previousCommands = new ArrayList();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.input = new System.Windows.Forms.TextBox();
			this.users = new System.Windows.Forms.ListBox();
			this.splitter1 = new System.Windows.Forms.Splitter();
			this.channelText = new System.Windows.Forms.RichTextBox();
			this.SuspendLayout();
			// 
			// input
			// 
			this.input.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.input.Location = new System.Drawing.Point(0, 817);
			this.input.Name = "input";
			this.input.Size = new System.Drawing.Size(760, 20);
			this.input.TabIndex = 0;
			this.input.Text = "";
			this.input.KeyUp += new System.Windows.Forms.KeyEventHandler(this.input_KeyUp);
			// 
			// users
			// 
			this.users.Dock = System.Windows.Forms.DockStyle.Right;
			this.users.IntegralHeight = false;
			this.users.Location = new System.Drawing.Point(640, 0);
			this.users.Name = "users";
			this.users.Size = new System.Drawing.Size(120, 817);
			this.users.TabIndex = 1;
			// 
			// splitter1
			// 
			this.splitter1.Dock = System.Windows.Forms.DockStyle.Right;
			this.splitter1.Location = new System.Drawing.Point(637, 0);
			this.splitter1.Name = "splitter1";
			this.splitter1.Size = new System.Drawing.Size(3, 817);
			this.splitter1.TabIndex = 2;
			this.splitter1.TabStop = false;
			// 
			// channelText
			// 
			this.channelText.Dock = System.Windows.Forms.DockStyle.Fill;
			this.channelText.Location = new System.Drawing.Point(0, 0);
			this.channelText.Name = "channelText";
			this.channelText.ReadOnly = true;
			this.channelText.Size = new System.Drawing.Size(637, 817);
			this.channelText.TabIndex = 3;
			this.channelText.Text = "";
			// 
			// Channel
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(760, 837);
			this.Controls.Add(this.channelText);
			this.Controls.Add(this.splitter1);
			this.Controls.Add(this.users);
			this.Controls.Add(this.input);
			this.Name = "Channel";
			this.Text = "Channel";
			this.Load += new System.EventHandler(this.Channel_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private void DosKey(bool up)
		{
			// todo: make sure that if we use a command, that we don't just add it to doskey, but we move it to a better position

			if (up)
			{
				currentPreviousCommandIndex++;
			}
			else
			{
				currentPreviousCommandIndex--;
			}
			if (currentPreviousCommandIndex >= previousCommands.Count || currentPreviousCommandIndex < 0)
			{
				currentPreviousCommandIndex = 0;
			}
			input.Text = (string)previousCommands[currentPreviousCommandIndex];
			
		}

		public void Say(string line)
		{
			// skriv in inneh�llet i kanalen
			// skicka det till servern
			server.SendMessage("PRIVMSG " + channelName + " :" + line);
			channelText.SelectionColor = Color.White;
			channelText.AppendText("[" + System.DateTime.Now.TimeOfDay.ToString().Substring(0, 8) + "] ");
			channelText.SelectionColor = Color.Blue;
			// todo: change this to the current nickname for this connection
			channelText.AppendText("<" + Config.Get("irc.nickname") + ">");
			channelText.SelectionColor = Color.Yellow;
			channelText.AppendText(line + Environment.NewLine);
		}

		public void Say(string message, User user)
		{
			channelText.SelectionColor = Color.White;
			channelText.AppendText("[" + System.DateTime.Now.TimeOfDay.ToString().Substring(0, 8) + "] ");
			channelText.SelectionColor = Color.Blue;
			channelText.AppendText("<" + user.Nickname + ">");
			channelText.SelectionColor = Color.Yellow;
			channelText.AppendText(message + Environment.NewLine);

			// eller kanske "User user", ha ett speciellt objekt eller n�tt, kanske, jag vet inte
			/*
			F�rgl�ggning:
			richTextBox1.SelectionFont=f;
			richTextBox1.AppendText(ff.GetName(1)+"\r\n");
			richTextBox1.SelectionFont=f;
			richTextBox1.AppendText("abcdefghijklmnopqrstuvwxyz\r\n");
			richTextBox1.SelectionFont=f;
			richTextBox1.AppendText("ABCDEFGHIJKLMNOPQRSTUVWXYZ\r\n");
			*/
		}

		public void Join(User user)
		{
			// add the user to the list
			// announce the event into the channel
		}

		public void Initiate()
		{
			// send a WHO command to the server to populate the userlist (or a names, depending on settings)
			
			// do this depending on setting.
			// we already get a names list on joining.

			server.SendMessage("WHO " + this.channelName);
			
		}

		private void input_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.Enter:
					// send the command
					if (input.Text.StartsWith("/"))
					{
						// this is a command, send it, without the /, to the server.
						// well, not really, we need to parse the command first to check what is happening
						HandleCommand(input.Text.Substring(1));
					} 
					else
					{
						Say(input.Text);
					}

					previousCommands.Add(input.Text);
					currentPreviousCommandIndex = previousCommands.Count;
					input.Clear();
					break;
				case Keys.Up:
					DosKey(true);
					break;
				case Keys.Down:
					DosKey(false);
					break;
			}
		}

		private void HandleCommand(string command)
		{
			switch (command)
			{
				case "clear":
					this.channelText.Clear();
					break;
				default:
					// send the command on up to the parent
					break;
			}
		}

		private void Channel_Load(object sender, System.EventArgs e)
		{
			// set bg and text color in all of the fields here.

		}
	}
}
