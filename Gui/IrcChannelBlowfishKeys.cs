using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Tiki.Gui
{
	/// <summary>
	/// Summary description for IrcChannelBlowfishKeys.
	/// </summary>
	public class IrcChannelBlowfishKeys : System.Windows.Forms.Form
	{
		private System.Windows.Forms.TextBox channel;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox key;
		private System.Windows.Forms.ListBox channels;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button cancel;
		private System.Windows.Forms.Button Ok;


		public IrcChannelBlowfishKeys()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(IrcChannelBlowfishKeys));
			this.channel = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.key = new System.Windows.Forms.TextBox();
			this.channels = new System.Windows.Forms.ListBox();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.cancel = new System.Windows.Forms.Button();
			this.Ok = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// channel
			// 
			this.channel.Location = new System.Drawing.Point(16, 40);
			this.channel.Name = "channel";
			this.channel.Size = new System.Drawing.Size(176, 20);
			this.channel.TabIndex = 0;
			this.channel.Text = "";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(16, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(176, 24);
			this.label1.TabIndex = 1;
			this.label1.Text = "Channel";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(200, 16);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(176, 24);
			this.label2.TabIndex = 3;
			this.label2.Text = "Key";
			// 
			// key
			// 
			this.key.Location = new System.Drawing.Point(200, 40);
			this.key.Name = "key";
			this.key.Size = new System.Drawing.Size(176, 20);
			this.key.TabIndex = 2;
			this.key.Text = "";
			// 
			// channels
			// 
			this.channels.Location = new System.Drawing.Point(16, 72);
			this.channels.Name = "channels";
			this.channels.Size = new System.Drawing.Size(176, 342);
			this.channels.Sorted = true;
			this.channels.TabIndex = 4;
			this.channels.SelectedIndexChanged += new System.EventHandler(this.channels_SelectedIndexChanged);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(200, 72);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(80, 24);
			this.button1.TabIndex = 5;
			this.button1.Text = "Add";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(200, 104);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(80, 24);
			this.button2.TabIndex = 6;
			this.button2.Text = "Update";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(200, 136);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(80, 24);
			this.button3.TabIndex = 7;
			this.button3.Text = "Remove";
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// cancel
			// 
			this.cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cancel.Location = new System.Drawing.Point(200, 392);
			this.cancel.Name = "cancel";
			this.cancel.Size = new System.Drawing.Size(80, 24);
			this.cancel.TabIndex = 9;
			this.cancel.Text = "Cancel";
			// 
			// Ok
			// 
			this.Ok.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.Ok.Location = new System.Drawing.Point(200, 360);
			this.Ok.Name = "Ok";
			this.Ok.Size = new System.Drawing.Size(80, 24);
			this.Ok.TabIndex = 8;
			this.Ok.Text = "Ok";
			// 
			// IrcChannelBlowfishKeys
			// 
			this.AcceptButton = this.Ok;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CancelButton = this.cancel;
			this.ClientSize = new System.Drawing.Size(392, 429);
			this.Controls.Add(this.cancel);
			this.Controls.Add(this.Ok);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.channels);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.key);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.channel);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "IrcChannelBlowfishKeys";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "IrcChannelBlowfishKeys";
			this.Load += new System.EventHandler(this.IrcChannelBlowfishKeys_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private void IrcChannelBlowfishKeys_Load(object sender, System.EventArgs e)
		{
			/*// populate the list with the info from the set.
			foreach (DictionaryEntry entry in Config.IrcChannelKeys)
			{
				channels.Items.Add(entry.Key.ToString().ToLower());
			}*/
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			/*Config.IrcChannelKeys.Add(channel.Text.ToLower(), key.Text);
			//channels.BeginUpdate();
			channels.Items.Add(channel.Text.ToLower());
			//channels.EndUpdate();*/
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			/*Config.IrcChannelKeys[channel.Text.ToLower()] = key.Text;*/
		}

		private void button3_Click(object sender, System.EventArgs e)
		{
			/*Config.IrcChannelKeys.Remove(channel.Text.ToLower());
			channels.BeginUpdate();
			channels.Items.Remove(channel.Text.ToLower());
			channels.EndUpdate();*/
		}

		private void channels_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			/*string s = (string)channels.SelectedItem;
			if (s != null) 
			{
				channel.Text = s.ToLower();
				key.Text = (string)Config.IrcChannelKeys[s];
			}*/
		
		}
	}
}
