using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace TikiRC.Gui
{
	/// <summary>
	/// Summary description for TikiRC.
	/// </summary>
	public class TikiRC : System.Windows.Forms.Form
	{
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.ToolBar toolBar1;
		private System.Windows.Forms.TreeView treeView1;
		private System.Windows.Forms.Splitter splitter1;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem windowsMenu;
		private System.Windows.Forms.MenuItem windowsTileMenu;
		private System.Windows.Forms.MenuItem windowsCascadeMenu;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public TikiRC()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			Config.Load();

			// should we load this with default info?
			StatusWindow sw = new StatusWindow();
			sw.MdiParent = this;
			sw.Show();

			/*Channel c = new Channel("#manwhores");
			c.MdiParent = this;
			c.Show();
			*/
			
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.windowsMenu = new System.Windows.Forms.MenuItem();
			this.windowsTileMenu = new System.Windows.Forms.MenuItem();
			this.windowsCascadeMenu = new System.Windows.Forms.MenuItem();
			this.toolBar1 = new System.Windows.Forms.ToolBar();
			this.treeView1 = new System.Windows.Forms.TreeView();
			this.splitter1 = new System.Windows.Forms.Splitter();
			this.SuspendLayout();
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem1,
																					  this.windowsMenu});
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 0;
			this.menuItem1.Text = "User Settings (temp)";
			this.menuItem1.Click += new System.EventHandler(this.menuItem1_Click);
			// 
			// windowsMenu
			// 
			this.windowsMenu.Index = 1;
			this.windowsMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						this.windowsTileMenu,
																						this.windowsCascadeMenu});
			this.windowsMenu.Text = "&Windows";
			// 
			// windowsTileMenu
			// 
			this.windowsTileMenu.Index = 0;
			this.windowsTileMenu.Text = "&Tile";
			this.windowsTileMenu.Click += new System.EventHandler(this.windowsTileMenu_Click);
			// 
			// windowsCascadeMenu
			// 
			this.windowsCascadeMenu.Index = 1;
			this.windowsCascadeMenu.Text = "&Cascade";
			this.windowsCascadeMenu.Click += new System.EventHandler(this.windowsCascadeMenu_Click);
			// 
			// toolBar1
			// 
			this.toolBar1.Appearance = System.Windows.Forms.ToolBarAppearance.Flat;
			this.toolBar1.ButtonSize = new System.Drawing.Size(16, 16);
			this.toolBar1.Divider = false;
			this.toolBar1.DropDownArrows = true;
			this.toolBar1.Location = new System.Drawing.Point(0, 0);
			this.toolBar1.Name = "toolBar1";
			this.toolBar1.ShowToolTips = true;
			this.toolBar1.Size = new System.Drawing.Size(760, 20);
			this.toolBar1.TabIndex = 3;
			// 
			// treeView1
			// 
			this.treeView1.Dock = System.Windows.Forms.DockStyle.Left;
			this.treeView1.ImageIndex = -1;
			this.treeView1.Location = new System.Drawing.Point(0, 20);
			this.treeView1.Name = "treeView1";
			this.treeView1.SelectedImageIndex = -1;
			this.treeView1.Size = new System.Drawing.Size(121, 905);
			this.treeView1.TabIndex = 4;
			// 
			// splitter1
			// 
			this.splitter1.Location = new System.Drawing.Point(121, 20);
			this.splitter1.Name = "splitter1";
			this.splitter1.Size = new System.Drawing.Size(3, 905);
			this.splitter1.TabIndex = 5;
			this.splitter1.TabStop = false;
			// 
			// TikiRC
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(760, 925);
			this.Controls.Add(this.splitter1);
			this.Controls.Add(this.treeView1);
			this.Controls.Add(this.toolBar1);
			this.IsMdiContainer = true;
			this.Menu = this.mainMenu1;
			this.Name = "TikiRC";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "TikiRC";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.TikiRC_Closing);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new TikiRC());
		}

		private void windowsTileMenu_Click(object sender, System.EventArgs e)
		{
			this.LayoutMdi(MdiLayout.TileHorizontal);
		}

		private void windowsCascadeMenu_Click(object sender, System.EventArgs e)
		{
			this.LayoutMdi(MdiLayout.Cascade);
		}

		private void menuItem1_Click(object sender, System.EventArgs e)
		{
			new Settings().ShowDialog();
		}

		private void TikiRC_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			Config.Save();
			// save other stuf here like window position and such
		}
	}
}
