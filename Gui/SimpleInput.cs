using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Tiki.Gui
{
	/// <summary>
	/// Summary description for SimpleInput.
	/// </summary>
	public class SimpleInput : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label data;
		private System.Windows.Forms.TextBox input;
		private System.Windows.Forms.Button okButton;
		private System.Windows.Forms.Button cancel;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;


		public SimpleInput(string message, string title, string defaultValue)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			this.Text = title;
			this.data.Text = message;
			this.input.Text = defaultValue;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.data = new System.Windows.Forms.Label();
			this.input = new System.Windows.Forms.TextBox();
			this.okButton = new System.Windows.Forms.Button();
			this.cancel = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// data
			// 
			this.data.Location = new System.Drawing.Point(8, 8);
			this.data.Name = "data";
			this.data.Size = new System.Drawing.Size(360, 56);
			this.data.TabIndex = 0;
			this.data.Text = "label1";
			// 
			// input
			// 
			this.input.Location = new System.Drawing.Point(8, 72);
			this.input.Name = "input";
			this.input.Size = new System.Drawing.Size(360, 20);
			this.input.TabIndex = 1;
			this.input.Text = "";
			this.input.KeyUp += new System.Windows.Forms.KeyEventHandler(this.input_KeyUp);
			// 
			// okButton
			// 
			this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.okButton.Location = new System.Drawing.Point(104, 104);
			this.okButton.Name = "okButton";
			this.okButton.TabIndex = 2;
			this.okButton.Text = "Ok";
			// 
			// cancel
			// 
			this.cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cancel.Location = new System.Drawing.Point(200, 104);
			this.cancel.Name = "cancel";
			this.cancel.TabIndex = 3;
			this.cancel.Text = "Cancel";
			// 
			// SimpleInput
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(376, 133);
			this.Controls.Add(this.cancel);
			this.Controls.Add(this.okButton);
			this.Controls.Add(this.input);
			this.Controls.Add(this.data);
			this.Name = "SimpleInput";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "SimpleInput";
			this.ResumeLayout(false);

		}
		#endregion

		private void input_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter)
			{
				this.okButton.PerformClick();
			}
		}

		public string Value
		{
			get
			{
				return this.input.Text;
			}
		}
	}
}
