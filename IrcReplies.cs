using System;

namespace TikiRC
{
	/// <summary>
	/// Summary description for IrcReplies.
	/// </summary>
	public class IrcReplies
	{
		// etc.
		// http://www.irchelp.org/irchelp/rfc/chapter6.html
		// also have a reverse lookup of these messages by int.


		public static int ERR_NOSUCHNICK = 401;
		public static int RPL_USERHOST = 302;
		/*
		 ":[<reply>{<space><reply>}]"
		 - Reply format used by USERHOST to list replies to the query list. The reply string is composed as follows:
		 <reply> ::= <nick>['*'] '=' <'+'|'-'><hostname>
		 The '*' indicates whether the client has registered as an Operator. The '-' or '+' characters represent whether the client has set an AWAY message or not respectively.
		 */

	}
}
