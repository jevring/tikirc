using System;
using System.Collections;
using System.Text.RegularExpressions;

namespace TikiRC
{
	/// <summary>
	/// Summary description for User.
	/// </summary>
	public class User
	{
		private string nickname;
		private string ident;
		private string host;
		//erudite!~captain@regulus3.student.UU.SE
		private static Regex whoLineSplitter = new Regex(@"^(.*)!(.*)@(.*)$");

		public User(string wholine)
		{
			Match m = whoLineSplitter.Match(wholine);
			if (m.Success)
			{
				nickname = m.Groups[1].ToString();
				ident = m.Groups[2].ToString();
				host = m.Groups[3].ToString();
			}
			else
			{
				throw new ArgumentException("wholine: " + wholine + " could not be parsed!");
			}
		}
		public User(params string[] whoResponseValues)
		{

		}

		public string Nickname
		{
			get { return nickname; }
		}

		public string Ident
		{
			get { return ident; }
		}

		public string Host
		{
			get { return host; }
		}
	}
}
